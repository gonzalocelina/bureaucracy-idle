/////////////// UTIL FUNCTIONS ///////////////

function elem(id) {
    return document.getElementById(id);
}

/////////////// DATABASE ///////////////
var resourcesTable = {
    money: { name: 'Money'},
    foodPermit: { name: 'Food permit'}
};
var buildingsTable = {
    hotDogStand: { name: 'Hotdog Stand', basePrice: { money: 50, foodPermit: 1 }, baseGain: { money: 1 }}
};
/////////////// GANE ///////////////

function NewGame() {
    this.resources =
        {
            money: {name: 'Money', amount: 50 },
            foodPermit: {name: 'Food permit', amount: 10}
        };
    this.buildings = {};
    for (var key in buildingsTable) {
        this.buildings[key] = new Building(key, 0)
    }
    this.setBuildings =
        function (buildingsData) {
            for (var key in buildingsData) {
                this.buildings[key] = new Building(key, buildingsData[key].owned)
            }
        };
    this.upgrades = {};
}
var Game = new NewGame();
function LoadGame(data) {
    Game.setBuildings(data.buildings);
    for (var key in data.resources) {
        Game.resources[key] = data.resources[key];
    }
}
if (window.localStorage.getItem('bureaucracySave')) {
    var data = JSON.parse(window.localStorage['bureaucracySave']);
    LoadGame(data);
}

/////////////// MODELS ///////////////

/**
 * Model for the Buildings
 *
 * @param name      => Name to display TODO: Make this translatable
 * @param owned     => Quantity owned (default = 0)
 * @param basePrice => Base price for all the resources as an array
 * @param baseGain  => Base resources earned by tick as an array
 * @constructor
 */
function Building(id, owned) {
    this.id = id;
    this.name = function () {
        return buildingsTable[id].name
    };
    this.owned = owned;
    this.getBasePrice = function () {
        return buildingsTable[id].basePrice;
    };
    this.getBaseGain = function () {
        return buildingsTable[id].baseGain;
    };
    this.bonuses = {};
    this.actualPrice =
        function (number = 1) {
            var total = {};
            for (i = 0; i < number; i++) {
                var expNumber = this.owned + i;
                for (var key in this.getBasePrice()) {
                    var resourceValue = this.getBasePrice()[key] * (Math.pow(1.1, expNumber));
                    total[key] = Math.round(resourceValue);
                }
            }

            return total;
        };
    this.calculateGains =
        function () {
            var gains = {};
            for (var key in this.getBaseGain()) {
                gains[key] = this.owned * this.getBaseGain()[key];
            }

            return gains;
        };
    this.formatPrice =
        function () {
            var formatedPrice = '';
            var labels = { //TODO: PASS this to the "database"
                money: '$',
                foodPermit: 'Food permits: ',
            }
            Object.entries(this.actualPrice()).forEach(([key, price]) => {
                formatedPrice += labels[key] + price + '<br>';
            });

            return formatedPrice;
        };
    this.addBonus =
        function (bonus) {
            Object.assign(this.bonuses, { bonus });
        };
}

/**
 * Model for the bonuses that the upgrades would grant.
 *
 * @param multiplier    => The muliplier as a decimal number (10% => 0.1)
 * @param resource      => What resource does the bonus affect
 *
 * These 2 are not necessary if it's a static bonus
 * @param ownedBuilding => The building.owned of the Building that affect the bonus.
 * @param divisor       => The number of buildings that grant the bonus (every 10, every 5, etc.).
 * @constructor
 */
function Bonus(multiplier, resource, ownedBuilding = 1, divisor = 1) {
    this.ownedBuilding = ownedBuilding;
    this.resource = resource;
    this.divisor = divisor;
    this.multiplier = multiplier;
    this.getTotalBonus = (this.ownedBuilding / this.divisor) * this.multiplier;
}

/**
 * Model for the Upgrades
 * 
 * @param name          => Name to display TODO: Make this translatable
 * @param description   => Description to display TODO: Make this translatable
 * @param bonuses       => Bonus objects as an array
 * @param price         => Price as an array
 * @constructor
 */
function Upgrade(name, description, bonuses, price) {
    this.name = name;
    this.description = description;
    this.bonuses = bonuses;
    this.price = price;
    this.owned = false;
}


/////////////// RESOURCE FUNCTIONS ///////////////
function earnResource(number, resource) {
    Game.resources[resource].amount = Game.resources[resource].amount + number;
}

function spendResource(number, resource) {
    Game.resources[resource].amount = Game.resources[resource].amount - number;
}

function updateListOfResources() {
//<div>$<span id="money">0</span></div>
    var resourcesDiv = elem('resources')
    var test = 0;
    for (var key in Game.resources) {
        var resource = Game.resources[key];
        var element = elem(key);
        if (element === null && resource.amount > 0) {
            resourcesDiv.innerHTML += '<div>' + resource.name + ': <span id="' + key + '">' + resource.amount + '</span>'
        }
    }

}


/////////////// BUILDING FUNCTIONS ///////////////
function updateBuildings() {
    for (var key in Game.buildings) {
        var building = Game.buildings[key];

        var element = elem(key);
        if (element) {
            if (building.owned > 0) {
                var spanValue = element.getElementsByClassName("value")[0];
                spanValue.innerHTML = '[' + building.owned + ']';
            }
            var spanPrice = element.getElementsByClassName("price")[0];
            spanPrice.innerHTML = building.formatPrice();
        }
    }
}

function buyBuilding(key, number = 1) {
    var building = Game.buildings[key];
    var total = building.actualPrice(number);

    var affordable = true;
    Object.entries(total).forEach(([resource, price]) => {
        if (Game.resources[resource].amount < price) {
            affordable = false;
        }
    });
    if (affordable) {
        building.owned = building.owned + number;

        Object.entries(total).forEach(([resource, price]) => {
            spendResource(price, resource);
        });
    }
}

/////////////// UPGRADE FUNCTIONS ///////////////

function buyUpgrade(key) {
    var upgrade = upgrades[key];
    var total = upgrade.price();

    var affordable = true;
    Object.entries(total).forEach(([resource, price]) => {
        if (Game.resources[resource].amount < price) {
            affordable = false;
        }
    });
    if (affordable) {
        upgrade.owned = true;

        Object.entries(upgrade.bonuses).forEach(([building, bonus]) => {
            Game.buildings[building].addBonus(bonus);
        });

        Object.entries(total).forEach(([resource, price]) => {
            spendResource(price, resource);
        });
    }
}

/////////////// GAINS ///////////////
function calculateGainsPerTick() {
    // Initialize all the values
    var gains = { //TODO: This should be an constant array that is with what you start
        money: 0,
        foodPermit: 0
    };
    for (var key in Game.buildings) {
        var buildingGains = Game.buildings[key].calculateGains();
        for (var i in buildingGains) {
            gains[i] = gains[i] + buildingGains[i];
        }
    }

    return gains;
}

function updateGains() {
    var gains = calculateGainsPerTick();

    for (var key in gains) {
        Game.resources[key].amount = Game.resources[key].amount + gains[key];

        var element = elem(key);
        if (element) {
            var content = Game.resources[key].amount;

            if (gains[key] > 0) {
                content = content + ' (+' + gains[key] + ' /tick)';
            }
            element.innerHTML = content;
        }
    }

}

/////////////// TIMER ///////////////
var counter= 0;
window.setInterval(function () {
    //updateResources();
    updateBuildings();
    updateGains();
    updateListOfResources();
    if (counter == 5) {
        window.localStorage['bureaucracySave'] = JSON.stringify(Game);
        counter = 0;
    }
    counter++;
}, 500);

$(document).ready(function () {
});

